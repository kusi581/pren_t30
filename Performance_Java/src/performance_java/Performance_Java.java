/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package performance_java;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import static org.opencv.imgcodecs.Imgcodecs.imread;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author Mathias Bänzigere
 */
public class Performance_Java {
    public String bild;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        File lib = new File(System.mapLibraryName("opencv_java300"));
        System.load(lib.getAbsolutePath());

        Performance_Java instance = new Performance_Java();
        
        String[] bilder = new String[4];
        
        bilder[0] = "1280x960.jpg";
        bilder[1] = "960x720.jpg";
        bilder[2] = "640x480.jpg";
        bilder[3] = "320x240.jpg";
        
        for(int i = 0; i<bilder.length; i++){
            instance.bild = bilder[i];
            System.out.println("\n" + bilder[i]+" ---------------------------------------------");      
            System.out.println("blur + Canny");
            instance.test1();
            System.out.println("blur + Canny + HoughLines");
            instance.test2();
            System.out.println("blur + Canny + HoughLinesP");
            instance.test3();
            System.out.println("blur + Canny + findContours");
            instance.test4();
        }         
        
    }
    
    public void test3(){
        int count = 30;
        int messungen = 0;
        int[] zeiten = new int[count];
        
        for (int i=0; i<count; i++){
            int start = Instant.now().getNano();
            
            
            Mat img = openImage();
            img =  smoothingImage(img, 3, 3);
            Mat edg = cannyEdgedetection(img, 100, 200);
            Mat lines = lineDetectionHoughLinesP(edg, 1, 1, 1, 1, 1);
            int end = Instant.now().getNano();
            if(end-start > 0){
                zeiten[i] = end-start;
                messungen++;
            }
        }
 
        
        double res = 0;
        for(int i = 0; i<zeiten.length; i++){
            res = res + zeiten[i];
        }
        res = res / 1000000000; // Nano to Sec
        double median = res/messungen;
        System.out.println("Durchschnittliche Zeit: "+median);
        System.out.println("Frequenz:               "+ 1/median + " Ausführungen/Sekunde");
        
    }
    
    
    public void test1(){
        //blur + Canny
        int count = 30;
        int messungen = 0;
        int[] zeiten = new int[count];
        for (int i=0; i<count; i++){
            int start = Instant.now().getNano();
            Mat img = openImage();
            img =  smoothingImage(img, 3, 3);
            Mat edg = cannyEdgedetection(img, 100, 200);
            int end = Instant.now().getNano();
            if(end-start > 0){
                zeiten[i] = end-start;
                messungen++;
            }
        }
        double res = 0;
        for(int i = 0; i<zeiten.length; i++){
            res = res + zeiten[i];
        }
        res = res / 1000000000; // Nano to Sec
        double median = res/messungen;
        System.out.println("Durchschnittliche Zeit: "+median);
        System.out.println("Frequenz:               "+ 1/median + " Ausführungen/Sekunde");  
    }
    
    public void test2(){
        int count = 30;
        int messungen = 0;
        int[] zeiten = new int[count];
        
        for (int i=0; i<count; i++){
            int start = Instant.now().getNano();
            Mat img = openImage();
            img =  smoothingImage(img, 3, 3);
            Mat edg = cannyEdgedetection(img, 100, 200);
            Mat lines = lineDetectionHoughLines(edg, 1, 1, 1);
            int end = Instant.now().getNano();
            if(end-start > 0){
                zeiten[i] = end-start;
                messungen++;
            }
        }
        double res = 0;
        for(int i = 0; i<zeiten.length; i++){
            res = res + zeiten[i];
        }
        res = res / 1000000000; // Nano to Sec
        double median = res/messungen;
        System.out.println("Durchschnittliche Zeit: "+median);
        System.out.println("Frequenz:               "+ 1/median + " Ausführungen/Sekunde");
    }
    
    private void saveImage(String path, Mat img){
        Imgcodecs.imwrite(path, img);
    }
    
    private Mat drawLineInImg(Mat img, Mat lines,int thickness){
        for(int i = 0; i < lines.cols(); i++) {
            double[] val = lines.get(0, i);
            Imgproc.line(img, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), thickness);
            //Imgproc.line(img,pint,pint,scalar,thicknes);
        }
        return img;        
    }
    

    /**
     * Diese Funktion erkennt Linien
     * Für diese Funktion, sollte ein Graustufenbild verwendet 
     * @param img Bild Matrix
     * @param rho The resolution of the parameter r in pixels. Bsp: 1 pixel.
     * @param theta The resolution of the parameter rho in radians. Bsp:(CV_PI/180)
     * @param threshold Minimalnummer an schnittpunkte die eine Linie haben muss
     * @param minLinLength Minimalnummer an Ecken welche zusammen eine Linie bilden können
     * @param maxLineGap Maximalabstand zwischen zwei Ecken um als Linie erkannt zu werden
     * @return Matrix bestehend aus erkanten  Linien -->  (x_{start}, y_{start}, x_{end}, y_{end})
     */
    private Mat lineDetectionHoughLinesP(Mat img, double rho, double theta, int threshold, double minLinLength, double maxLineGap){
        Mat lines = new Mat();
        Imgproc.HoughLinesP(img, lines, rho, theta, threshold, minLinLength, maxLineGap);
       
        return lines;       
    }
    
    private Mat cannyEdgedetection(Mat img, double sizeA, double sizeB){
        Mat edg = new Mat();
        Imgproc.Canny(img, edg, sizeA, sizeB);
        return edg;
    }
    
    private Mat smoothingImage(Mat img,int sizeA, int sizeB){
        Imgproc.blur(img, img, new Size(sizeA, sizeB));
        return img;
    }
    
    private Mat openImage(){
        File path = new File("src/performance_java/"+this.bild);
        String stringPath = path.getPath();
        return Imgcodecs.imread(stringPath);
    }
    
    private Image mat2Image(Mat frame) throws IOException{
        MatOfByte buffer = new MatOfByte();
        Imgcodecs.imencode(".png", frame, buffer);
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }
    


    private Mat lineDetectionHoughLines(Mat img, double rho, double theta, int threshold){
        Mat lines = new Mat();
        Imgproc.HoughLines(img, lines, rho, theta, threshold);
       
        return lines;       
    }
    
    
    public void test4(){
        int count = 30;
        int messungen = 0;
        int[] zeiten = new int[count];
        
        for (int i=0; i<count; i++){
            int start = Instant.now().getNano();
 
            Mat img = openImage();
            img =  smoothingImage(img, 3, 3);
            Mat edg = cannyEdgedetection(img, 100, 200);
            Mat lines = lineDetectionFindContours(edg, new Mat(), 1, 1);
           
            
            int end = Instant.now().getNano();
            if(end-start > 0){
                zeiten[i] = end-start;
                messungen++;
            }
        }
        double res = 0;
        for(int i = 0; i<zeiten.length; i++){
            res = res + zeiten[i];
        }
        res = res / 1000000000; // Nano to Sec
        double median = res/messungen;
        System.out.println("Durchschnittliche Zeit: "+median);
        System.out.println("Frequenz:               "+ 1/median + " Ausführungen/Sekunde");
        
    }
    
    private Mat lineDetectionFindContours(Mat img, Mat hierarchy, int mode, int method){
        Mat lines = new Mat();
        List<MatOfPoint> contours = new ArrayList<>();    
        Imgproc.findContours(img, contours, hierarchy, mode, method);
        return lines;       
    }
}


/*
    Testfälle:
    I) blur + Canny
    II)     ""      + HoughLines
    III)     ""     + HoghLinesP
    IV)     ""      + findContours

*/
