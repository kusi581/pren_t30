/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pern_linedetection;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgproc.Imgproc;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;

/**
 * FXML Controller class
 *
 * @author Mathias Bänzigere
 */
public class LineDetectionController {
    // the FXML button
    @FXML
    private Button button;
    @FXML
    private ImageView currentFrame;
    @FXML
    private ImageView frame1;
    @FXML
    private ImageView frame2;
    @FXML
    private Label CannyThreshold1;
    @FXML
    private Label CannyThreshold2;
    
    //Canny
    @FXML
    private Slider SliderCannyThreshold1;
    @FXML
    private Slider SliderCannyThreshold2;
    
    //Blur
    @FXML
    private Slider SliderBlur1;
    @FXML
    private Slider SliderBlur2;
    
    //HoughLinesP
    @FXML
    private TextField HoughLinesPrho;
    @FXML
    private TextField HoughLinesPtheta;
    @FXML
    private Slider HoughLinesPthreshold; 
   @FXML
    private Slider HoughLinesPminLinLength; 
   @FXML
    private Slider HoughLinesPmaxLineGap; 
   
    
    
            
    
    
    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    // a flag to change the button behavior
    private boolean cameraActive = false;

    public LineDetectionController() {
        /*
         SliderCannyThreshold1.valueProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
            if (newValue == null) {
              CannyThreshold1.setText("");
              return;
            }
            CannyThreshold1.setText(Math.round(newValue.intValue()) + "");
            }
          });
         */
      
    }
    

    @FXML
    protected void startCamera(ActionEvent event){	
        if (!this.cameraActive){ 
            this.capture.open(0); //start the video capture	

            if (this.capture.isOpened()){ // is the video stream available?
                this.cameraActive = true;

                Runnable frameGrabber;
                frameGrabber = new Runnable(){
                    @Override
                    public void run(){
                        Image imageToShow = grabFrame();
                        currentFrame.setImage(imageToShow);
                    }
                };

                this.timer = Executors.newSingleThreadScheduledExecutor();
                this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);

                // update the button content
                this.button.setText("Stop Camera");
            }else{
                System.err.println("Impossible to open the camera connection...");
            }
        }else{
            // the camera is not active at this point
            this.cameraActive = false;
            // update again the button content
            this.button.setText("Start Camera");

            // stop the timer
            try{
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            }catch (InterruptedException e){
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }

            // release the camera
            this.capture.release();
            // clean the frame
            this.currentFrame.setImage(null);
        }
    }

    /**
     * Get a frame from the opened video stream (if any)
     * 
     * @return the {@link Image} to show
     */
    private Image grabFrame(){
        // init everything
        Image imageToShow = null;
        Mat frame = new Mat();

        // check if the capture is open
        if (this.capture.isOpened()){
            try{
                // read the current frame
                this.capture.read(frame);
                if (!frame.empty()){
                    // convert the image to gray scale
                    Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
                    // convert the Mat object (OpenCV) to Image (JavaFX)
                    imageToShow = mat2Image(frame);
                }
            }catch (Exception e){
                System.err.println("Exception during the image elaboration: " + e);
            }
        }
        return imageToShow;
    }
	
    /**
     * Convert a Mat object (OpenCV) in the corresponding Image for JavaFX
     * 
     * @param frame
     *            the {@link Mat} representing the current frame
     * @return the {@link Image} to show
     */
    private Image mat2Image(Mat frame) throws IOException{
        MatOfByte buffer = new MatOfByte();
        Imgcodecs.imencode(".png", frame, buffer);
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }

    @FXML
    public void test(){
            
            String path = "C:\\temp\\2.jpg";
            System.out.println("Open: "+path);
            Mat img = Imgcodecs.imread(path);
            try {
                //currentFrame.setImage(mat2Image(img));
                
                // generate gray scale and blur
		Mat gray = new Mat();
		Imgproc.cvtColor(img, gray, Imgproc.COLOR_BGR2GRAY);
                System.out.println("ccc");
                //currentFrame.setImage(mat2Image(gray));

		Imgproc.blur(gray, gray, new Size(3, 3));
                //currentFrame.setImage(mat2Image(gray));

                System.out.println("ddd");
                
                // detect the edges
		Mat edges = new Mat();
		int lowThreshold = 50; //   <---- hier schauen!!
		int ratio = 3;         //   <---- hier schauen!!!
                System.out.println("eee");

                Imgproc.Canny(gray, edges, 100, 300);
		//Imgproc.Canny(img, edges, lowThreshold, lowThreshold * ratio);
		System.out.println("fff");
                
		Mat lines = new Mat();
               
		Imgproc.HoughLinesP(edges, lines, 1, Math.PI / 180, 50, 50, 10);
		System.out.println("ggg");
		for(int i = 0; i < lines.cols(); i++) {
			double[] val = lines.get(0, i);
			Imgproc.line(img, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), 2);
		}
                System.out.println("hhh");
                

            frame1.setImage(mat2Image(img));
            
            frame2.setImage(mat2Image(gray));
            
            currentFrame.setImage(mat2Image(edges));
                
            //Imgcodecs.imwrite("resources/compiled/1.jpg", lines);
            
            //ImgWindow.newWindow(edges);
            //ImgWindow.newWindow(gray);
            //ImgWindow.newWindow(img);


            } catch (Exception ex) {
                System.out.println(ex.getMessage());            }
        }
        
        
        
    /**
     * Diese Funktion erkennt Linien
     * Für diese Funktion, sollte ein Graustufenbild verwendet 
     * @param img Bild Matrix
     * @param rho The resolution of the parameter r in pixels. Bsp: 1 pixel.
     * @param theta The resolution of the parameter rho in radians. Bsp:(CV_PI/180)
     * @param threshold Minimalnummer an schnittpunkte die eine Linie haben muss
     * @param minLinLength Minimalnummer an Ecken welche zusammen eine Linie bilden können
     * @param maxLineGap Maximalabstand zwischen zwei Ecken um als Linie erkannt zu werden
     * @return Matrix bestehend aus erkanten  Linien -->  (x_{start}, y_{start}, x_{end}, y_{end})
     */
    private Mat lineDetectionHoughLinesP(Mat img, double rho, double theta, int threshold, double minLinLength, double maxLineGap){
        Mat lines = new Mat();
        Imgproc.HoughLinesP(img, lines, rho, theta, threshold, minLinLength, maxLineGap);
       
        return lines;       
    }
    /**
     * 
     * @param img
     * @param sizeA Grerenzwert1 für Hysterese
     * @param sizeB Grerenzwert1 für Hysterese
     * @return 
     */
    private Mat cannyEdgedetection(Mat img, double sizeA, double sizeB){
        Mat edg = new Mat();
        Imgproc.Canny(img, edg, sizeA, sizeB);
        
                
        return edg;
    }
    
    private Mat smoothingImage(Mat img,int sizeA, int sizeB){
        Imgproc.blur(img, img, new Size(sizeA, sizeB));
        return img;
    }
    
    private Mat openImage(String bild){
        File path = new File("src/pern_linedetection/"+bild);
        String stringPath = path.getPath();
        return Imgcodecs.imread(stringPath);
    }
    
 
    private Mat lineDetectionHoughLines(Mat img, double rho, double theta, int threshold){
        Mat lines = new Mat();
        Imgproc.HoughLines(img, lines, rho, theta, threshold);
       
        return lines;       
    }
    
    
    private Mat lineDetectionFindContours(Mat img, Mat hierarchy, int mode, int method){
        Mat lines = new Mat();
        List<MatOfPoint> contours = new ArrayList<>();    
        Imgproc.findContours(img, contours, hierarchy, mode, method);
        return lines;       
    }
    
    @FXML
    private void changeInSliderCannyThreshold1(){
        double value = SliderCannyThreshold1.getValue();     
        this.CannyThreshold1.setText(Double.toString(value));
        System.out.println("value: " +Double.toString(value));
        
    }
    
    @FXML
    private void changeInSliderCannyThreshold2(){
        double value = SliderCannyThreshold2.getValue();     
        this.CannyThreshold2.setText(Double.toString(value));
         System.out.println("value:");
    }
    
    @FXML
    private void testingAlgo() throws IOException{
        Mat img = openImage("1280x960.jpg");
        System.out.println(SliderBlur1.getValue());
        img =  smoothingImage(img,(int) SliderBlur1.getValue(), (int)SliderBlur2.getValue());
       // img = smoothingImage(img, 3,3);
        frame1.setImage(mat2Image(img));
        Mat edg = cannyEdgedetection(img, SliderCannyThreshold1.getValue(), SliderCannyThreshold2.getValue());
        frame2.setImage(mat2Image(edg));
         
        Mat lines = lineDetectionHoughLinesP(edg,Double.valueOf(HoughLinesPrho.getText()),Double.valueOf(HoughLinesPtheta.getText()),(int) HoughLinesPthreshold.getValue(),(double) HoughLinesPminLinLength.getValue(),(double) HoughLinesPmaxLineGap.getValue());
        for(int i = 0; i < lines.cols(); i++) {
            double[] val = lines.get(0, i);
            Imgproc.line(img, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), 2);
        }
        currentFrame.setImage(mat2Image(img));
        
    }
}
