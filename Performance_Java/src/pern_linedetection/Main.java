/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pern_linedetection;

import java.io.File;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;
import org.opencv.core.Core;


/**
 *
 * @author Mathias Bänzigere
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage){
            try{
                FXMLLoader loader = new FXMLLoader(getClass().getResource("LineDetection.fxml"));

                BorderPane rootElement = (BorderPane) loader.load();
                //Scene scene = new Scene(rootElement, 450, 450);
                Scene scene = new Scene(rootElement);

                primaryStage.setTitle("Cam abcdef"); 
                primaryStage.setScene(scene);
                primaryStage.show();
                
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
	}
	
	/**
	 * For launching the application...
	 * 
	 * @param args
	 *            optional params
	 */
	public static void main(String[] args){
            File lib = new File(System.mapLibraryName("opencv_java300"));
            System.load(lib.getAbsolutePath());
            
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            launch(args);
            
	}
}