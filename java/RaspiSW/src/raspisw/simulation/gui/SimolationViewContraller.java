package raspisw.simulation.gui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import raspisw.logic.BasicLogger;
import raspisw.logic.Logic;

public class SimolationViewContraller implements Initializable {
	private Logic logic = Logic.getInstance(new CommunicationMock(this),new CameraMock(this), new ResultMock()); 
	private ArrayList<String> logs;
	
	@FXML
	private Button b1;
	@FXML
	private ImageView image1;
	@FXML
	private Label label;
	

	@FXML
	public void b1_pressd(){
		this.logs.clear();
		logic.setLogicProcessingRateInMs(1000);
		logic.run();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logs = new ArrayList<>();
		BasicLogger.workWithSimolationGUI(this);
		BasicLogger.addLog("Simolation: started");
		
	}
	
	private Image mat2Image(Mat frameSim) throws IOException{
        MatOfByte buffer = new MatOfByte();
        Imgcodecs.imencode(".png", frameSim, buffer);
        return new Image(new ByteArrayInputStream(buffer.toArray()));
	}
	
	
	public void setImageToGUI(Mat imgSIm){
		try {
			Platform.runLater(new Runnable(){
				@Override
				public void run() {
					try {
						image1.setImage(mat2Image(imgSIm));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});			
		}catch(Exception e ){
			System.out.println(e.getMessage());
		}
	}
	
	public void setLogToGUI(String log){
		try {
			Platform.runLater(new Runnable(){
				@Override
				public void run() {
					try {
						if(logs.size()<35){
							logs.add(log);
						}else{
							logs.remove(0);
							logs.add(log);
						}
						String text = "";
						for(String s: logs){
							text = text + s + "\n";
						}
						label.setText(text);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});			
		}catch(Exception e ){
			System.out.println(e.getMessage());
		}
	}
	

}
