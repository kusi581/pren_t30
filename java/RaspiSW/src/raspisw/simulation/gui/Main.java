package raspisw.simulation.gui;

import org.opencv.core.Core;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import raspisw.logic.BasicLogger;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME); 
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("SimolationView.fxml"));

			BorderPane rootElement = (BorderPane) loader.load();
			Scene scene = new Scene(rootElement,1200,700);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		BasicLogger.startSavingInterval(10);
		BasicLogger.addLog("Main gestartet");
		launch(args);
		BasicLogger.stopLogger();
	}
}

