package raspisw.simulation.gui;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;


import raspisw.logic.ICamera;

public class CameraMock implements ICamera {
	 public ArrayList<String> files;
	 public int filesCount;
	 private SimolationViewContraller gui;
	 
	 public CameraMock(){
	 }
	 
	 CameraMock(SimolationViewContraller gui){
		 //this.files = getFilesInPath("./images/");
	   this.files = getFilesInPath("C://ImgPren//");
		 this.filesCount = 0;
		 this.gui = gui;
	 }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startCapture() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setShouldSaveImages(boolean shouldSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stopCapture() {
		// TODO Auto-generated method stub
		
	}
  Mat imgSim = new Mat();

	@Override
	public Mat getCurrentImage() {
		if (gui == null)
			return new Mat();
		
		if (filesCount < files.size()){
			filesCount++;
		}else{
			filesCount = 0;
		}
		imgSim.release();
		imgSim = Imgcodecs.imread(files.get(filesCount));
		gui.setImageToGUI(imgSim);
		return imgSim;
	}

    private ArrayList<String> getFilesInPath(String path){
    	ArrayList<String> files = new ArrayList<>();
    	try {
			Files.walk(Paths.get(path)).forEach(filePath -> {
			    if (Files.isRegularFile(filePath)) {
			        
			        files.add(""+filePath);
			    }
			});
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return files;
    }
}
