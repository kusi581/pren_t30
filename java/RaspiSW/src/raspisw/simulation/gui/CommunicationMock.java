package raspisw.simulation.gui;

import raspisw.communication.ICommunication;
import raspisw.communication.IGPIOListener;
import raspisw.communication.State;
import raspisw.logic.BasicLogger;

public class CommunicationMock implements ICommunication {
	private final SimolationViewContraller gui;

	public CommunicationMock() {
		gui = null;
	}
	
	CommunicationMock(final SimolationViewContraller gui) {
		this.gui = gui;
	}

	@Override
	public void sendDirection(final State motorSignal) {
		BasicLogger.addLog("Communication: " + motorSignal.toString());

	}

	@Override
	public void sendSteering(final int angle) {
		BasicLogger.addLog("Communication: steering " + (angle));
	}

	@Override
	public void sendContainer() {
		// TODO Auto-generated method stub
		System.out.println("Communication: Container");

	}

	@Override
	public void addGPIOListener(final IGPIOListener gpioListener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDefaultPinState() {
		// TODO Auto-generated method stub

	}

}
