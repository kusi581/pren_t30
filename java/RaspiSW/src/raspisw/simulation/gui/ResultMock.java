package raspisw.simulation.gui;

import java.util.List;

import detection.car.CarResult;
import detection.container.ContainerResult;
import detection.dump.DumpResult;
import detection.line.LineResult;
import raspisw.imageprocessing.IResult;

public class ResultMock implements IResult{

	private double currentAngle = -65;
	private double currentxPos = 0.4;
	
	@Override
	public List<LineResult> getLines() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ContainerResult> getContainers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CarResult> getCars() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LineResult getNewestLine() {
		LineResult result = new LineResult(currentAngle, currentxPos);
		if (currentAngle == 64)
			throw new RuntimeException("finish now");
		if (currentAngle < -90)
			currentAngle = 91;
		currentAngle -= 1;
		return result;
	}

	@Override
	public ContainerResult getNewestContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarResult getNewestCar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DumpResult getNewestDump() {
		// TODO Auto-generated method stub
		return null;
	}

}
