package raspisw.imageprocessing;

import java.util.List;

import detection.DetectionResults;
import detection.IDetectionResults;
import detection.car.CarResult;
import detection.container.ContainerResult;
import detection.dump.DumpResult;
import detection.line.LineResult;

public class Result implements IResult {
	private static IResult instance;
	private IDetectionResults detectionResults = DetectionResults.getInstance();

	private Result() {
	}

	public static IResult getInstance() {
		return instance == null ? instance = new Result() : instance;
	}

	@Override
	public List<LineResult> getLines() {
		return detectionResults.getLines();
	}

	@Override
	public List<ContainerResult> getContainers() {
		return detectionResults.getContainers();
	}

	@Override
	public List<CarResult> getCars() {
		return detectionResults.getCars();
	}

	@Override
	public LineResult getNewestLine() {
		return detectionResults.getNewestLine();
	}

	@Override
	public ContainerResult getNewestContainer() {
		return detectionResults.getNewestContainer();
	}

	@Override
	public CarResult getNewestCar() {
		return detectionResults.getNewestCar();
	}

	@Override
	public DumpResult getNewestDump() {
		return detectionResults.getNewestDump();
	}
}
