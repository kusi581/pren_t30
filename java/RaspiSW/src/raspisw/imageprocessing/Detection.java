package raspisw.imageprocessing;

import org.opencv.core.Mat;

import detection.IObjectDetection;
import detection.ObjectDetection;

public class Detection implements IDetection {
	private static IDetection instance;
	private IObjectDetection objectDetection;

	private Detection() {
		objectDetection = new ObjectDetection();
	}

	public static IDetection getInstance() {
		return instance == null ? instance = new Detection() : instance;
	}

	@Override
	public void detect(Mat img, DetectionType type) {
		if (img != null) {
			switch (type) {
			case CAR:
				objectDetection.detectCar(img);
				break;
			case CONTAINER:
				objectDetection.detectContainer(img);
				break;
			case LINE:
				objectDetection.detectLines(img);
				break;
			case DUMP:
				objectDetection.detectDump(img);
				break;
			}
		}
	}
}
