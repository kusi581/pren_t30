package raspisw.imageprocessing;

import java.util.List;

import detection.car.CarResult;
import detection.container.ContainerResult;
import detection.dump.DumpResult;
import detection.line.LineResult;

public interface IResult {
	public List<LineResult> getLines();

	public List<ContainerResult> getContainers();

	public List<CarResult> getCars();

	public DumpResult getNewestDump();

	public LineResult getNewestLine();

	public ContainerResult getNewestContainer();

	public CarResult getNewestCar();
}
