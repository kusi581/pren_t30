package raspisw.imageprocessing;

import org.opencv.core.Mat;

public interface IDetection {
	public void detect(Mat img, DetectionType type);

}
