package raspisw.logic;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class BasicLoggerTest {

	@Test
	public void addLogTest1() { //�berpr�ft die Anzahl der Loggs
		ArrayList<LoggList> loggs = BasicLogger.getLogs();
		int sizeBeginn = loggs.size();		
		BasicLogger.addLog("aa");
		BasicLogger.addLog("bb");
		loggs = BasicLogger.getLogs();
		int sizeEnd = loggs.size();
		assertEquals(sizeEnd-sizeBeginn,2);
	}
	
	@Test
	public void addLogTest2() { // �berpr�ft den letzten Logg
		BasicLogger.addLog("Hallo");
		ArrayList<LoggList> loggs = BasicLogger.getLogs();
		assertEquals(loggs.get(loggs.size()-1).getMessage(),"Hallo");
		
	}
	

}
