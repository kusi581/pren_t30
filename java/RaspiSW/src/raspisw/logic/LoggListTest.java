package raspisw.logic;

import static org.junit.Assert.*;

import java.time.Instant;

import org.junit.Before;
import org.junit.Test;

public class LoggListTest {
	private LoggList instance;
	
	
	@Before
	public void init() {
		instance = new LoggList("Test Nachricht!");
	}
	

	@Test
	public void testGetInstant() {
		boolean res = false;
		Instant date = instance.getInstant();
		if (date != null){
			res = true;
		}
		assertTrue(res);
	}

	@Test
	public void testGetMessage() {
		assertEquals("Test Nachricht!",instance.getMessage());
	}
	
	@Test
	public void compareTimestamp(){
		LoggList instance1 = new LoggList("Hallo");
		Instant time1 = instance1.getInstant();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		LoggList instance2 = new LoggList("Hallo");
		Instant time2 = instance2.getInstant();
		boolean res = time1.isBefore(time2);
		assertTrue(res);
	}

}
