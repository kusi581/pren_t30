package raspisw.logic;

import java.time.Instant;
import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import raspisw.simulation.gui.SimolationViewContraller;

public class BasicLogger {
	private static ArrayList<LoggList> loggs = new ArrayList<>();
	private static final Logger log = Logger.getLogger(BasicLogger.class);
	private static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(0);
	private static boolean isSavingThreadRunning = false;
	private static SimolationViewContraller svc;

	/**
	 * addLog: F�gt der ArrayList einen neuen Log hinzu.
	 * 
	 * @param message
	 */
	public static void addLog(final String message) {
		final LoggList log = new LoggList(message);
		synchronized (loggs) {

			// loggs.add(log);
			if (svc != null) {
				svc.setLogToGUI(message);
			}

			System.out.println(log.getInstant().toString() + " - " + message);
		}
	}

	/**
	 * getLogs: Gibt ein ArraList mit allen Logs zur�ck.
	 * 
	 * @return ArrayList<LoggList>
	 */
	public static ArrayList<LoggList> getLogs() {
		synchronized (loggs) {
			return loggs;
		}
	}

	/**
	 * saveLoggs: Schreibt alle Logs in der ArrayList in das Log File und leert
	 * die ArrayList.
	 */
	private static void saveLoggs() {
		ArrayList<LoggList> clonedLoggs;
		synchronized (loggs) {
			clonedLoggs = (ArrayList<LoggList>) loggs.clone();
			loggs.clear();
		}
		for (int i = 0; i < clonedLoggs.size(); i++) {
			final Instant timestamp = clonedLoggs.get(i).getInstant();
			final String message = clonedLoggs.get(i).getMessage();
			//log.info(timestamp.toString() + " - " + message);
		}
	}

	public static void startSavingInterval(final int seconds) {
		if (isSavingThreadRunning == false) {
			final SavingThread saver = new SavingThread();
			executor.scheduleAtFixedRate(saver, 0, seconds, TimeUnit.SECONDS);
			isSavingThreadRunning = true;
			BasicLogger.addLog("LOGGER: Logger started");
		}
	}

	public static void stopLogger() {
		if (isSavingThreadRunning) {
			BasicLogger.addLog("LOGGER: Logger is stoping");
			executor.shutdown();
			isSavingThreadRunning = false;
			saveLoggs();
		}
	}

	private static class SavingThread implements Runnable {
		@Override
		public void run() {
			BasicLogger.saveLoggs();
		}
	}

	// zu Testzwecken
	public static void main(final String[] Args) {
		BasicLogger.addLog("Was geht ab?");
		BasicLogger.addLog("Nicht viel!");
		BasicLogger.addLog("Whats Uuuuuuuuuuup!");

		BasicLogger.startSavingInterval(1);
		try {
			Thread.sleep(6000); // 6 sec warten
		} catch (final InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BasicLogger.addLog("2. Durchlauf!!!");

	}

	public static void workWithSimolationGUI(final SimolationViewContraller svc) {
		BasicLogger.svc = svc;

	}

}
