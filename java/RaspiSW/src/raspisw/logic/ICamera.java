package raspisw.logic;

import org.opencv.core.Mat;

public interface ICamera extends Runnable {
	public void startCapture();
	public void setShouldSaveImages(boolean shouldSave);
	public void stopCapture();
	public Mat getCurrentImage();
}
