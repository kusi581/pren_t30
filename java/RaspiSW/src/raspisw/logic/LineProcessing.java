package raspisw.logic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.internal.runners.ErrorReportingRunner;

import detection.ConfiguruartionValueHolder;
import detection.line.LineResult;
import raspisw.communication.ICommunication;
import raspisw.communication.State;

public class LineProcessing {

	// references
	private ICommunication communication;
	private PIDController pid;
	private PIDController pidStraight;
	private PIDController pidLeft;
	private PIDController pidRight;

	// angle
	private final int rawAngleCorrection = -4;
	private final double angleDividend = 2.33333333333d;
	private final int maxAngleDiff = 8;
	private int lastAngle = 0;

	// linePos
	private final int linePosFactor = 1000;
	private final int idealLinePos = 400;
	private final int maxLinePosDiff = 300;
	private final int linePosOffsetRight = -200;
	private final int linePosOffsetLeft = 200;
	private int lastLinePos = idealLinePos;
	private int currentLinePos = idealLinePos;

	// steering and speed
	private int lastFinalSteering = 0;
	private int maxFinalSteeringDiff = 50;
	private final int minStraight = 2;
	private final int minCurveLeft = 2;
	private final int minCurveRight = 2;
	private int curveLeftCounter = 0;
	private int curveRightCounter = 0;
	private int straightCounter = ConfiguruartionValueHolder.isUSE_LEFT_START() ? minStraight : 0;
	private final int finalSteeringDividend = 17;
	private int errorCounter = 0;
	private final int maxErrorCounter = 20;

	public LineProcessing(ICommunication communication) {
		this.communication = communication;
		pidStraight = new PIDController(0, 0.4, 0, 0.4);
		pidLeft = new PIDController(0, 0.5, 0, 0.5);
		pidRight = new PIDController(0, 0.5, 0, 0.5);
		pid = pidStraight;
	}

	public void process(LineResult result) {

		double rawAngle = result.getAngle(); // -90...-50 -> left, 90...50 ->
												// right
		double rawLinePos = result.getxPosOfLine(); // 0...1

		if (errorCounter > maxErrorCounter) {
			// lastLinePos = idealLinePos;
			// lastFinalSteering = 0;
			// lastAngle = 0;
			// communication.sendDirection(State.SLOW);
			// errorCounter = 0;
		}

		// check if values are valid, return if invalid
		if ((rawAngle < 50 && rawAngle > -50) || (rawLinePos < 0 || rawLinePos > 1)) {
			BasicLogger.addLog("LineProcessing: Code RED -> invalid value(s)");
			error();
			return;
		}

		// process angle and linePos
		int angle = processAngle((int) rawAngle);
		int linePos = processLinePos(rawLinePos);

		// check linePos, return if invalid
		if (!isLinePosValid(linePos)) {
			BasicLogger.addLog(
					"LineProcessing: Code BLUE -> invalid line pos (pos=" + linePos + "; lpos=" + lastLinePos + ")");
			error();
			return;
		}

		// check angle, set angle as last angle if invalid
		if (!isAngleValid(angle)) {

			BasicLogger.addLog(
					"LineProcessing: Code RAINBOW -> invalid angle (angle=" + angle + "; lastangle=" + lastAngle + ")");
			error();// angle = lastAngle;
			return;
		}
	

	// change current line pos according to the angle
	changeCurrentLinePos(angle);

		// calculates the final steering value and validates
		int finalSteering = calcFinalSteering(angle, linePos);

		int pidValue;
		Double pidResult = pid.update(System.nanoTime(), finalSteering);
		if (pidResult != null)
			pidValue = (int) -pidResult;
		else
			pidValue = (int) pid.getSetPoint();

		if (!isFinalSteeringValid(pidValue)) {
			BasicLogger.addLog("LineProcessing: Code GREEN -> final steering invalid (" + lastFinalSteering + " -> "
					+ pidValue + ")");
			error();
			return;
		}

		errorCounter = 0;

		//BasicLogger.addLog("LineProcessing: " + finalSteering + " -> " + pidValue);
		
		BasicLogger.addLog("LineProcessing: raw=" + angle + ";final=" + finalSteering + ";clp=" + currentLinePos
				+ ";diff=" + (currentLinePos - linePos) + ";pid=" + pidValue);

		// communication: speed and steering
		changeSpeed(finalSteering);
		communication.sendSteering(pidValue + 16);

		// set lastAngle and lastLinePos
		lastAngle = angle;
		lastLinePos = linePos;
		lastFinalSteering = pidValue;
	}

	private void error() {
		errorCounter++;
		// communication.sendDirection(State.SLOW);
	}

	private int processAngle(int rawAngle) {
		int angle;
		// to positive angle
		if (rawAngle < 0)
			angle = Math.abs(rawAngle);
		else
			angle = 90 + (90 - rawAngle);

		// add offset
		angle += rawAngleCorrection;

		// to steering value [-17...17]
		return (int) Math.round((-90 + angle) / angleDividend);
	}

	/**
	 * 
	 * @param rawLinePos
	 *            the raw line position [0...1]
	 * @return current line position
	 */
	private int processLinePos(double rawLinePos) {
		return ((int) (rawLinePos * linePosFactor));
	}

	/**
	 * 
	 * @return whether angle is valid or not (compared to last)
	 */
	private boolean isAngleValid(int angle) {
		return Math.abs(angle - lastAngle) < maxAngleDiff;
	}

	/**
	 * 
	 * @return whether the line position is valid or not (compared to last)
	 */
	private boolean isLinePosValid(int linePos) {
		return Math.abs(linePos - lastLinePos) < maxLinePosDiff;
	}

	private boolean isFinalSteeringValid(int finalSteering) {
		return Math.abs(finalSteering - lastFinalSteering) < maxFinalSteeringDiff;
	}

	private void changeCurrentLinePos(int angle) {
		if (angle > 5) { // rechts
			curveRightCounter++;
			if (curveRightCounter > minCurveRight) {
				currentLinePos = idealLinePos + linePosOffsetRight;
				if (pid != pidRight) {
					pid = pidRight;
					pid.setSetpoint(0);
					BasicLogger.addLog("PIDRIGHT");
				}
			}

			straightCounter = 0;
			curveLeftCounter = 0;
		} else if (angle < -5) { // links
			curveLeftCounter++;

			if (curveLeftCounter > minCurveLeft) {
				if (pid != pidLeft) {
					pid = pidLeft;
					pid.setSetpoint(0);
					BasicLogger.addLog("PIDLEFT");
				}
				currentLinePos = idealLinePos + linePosOffsetLeft;
			}

			straightCounter = 0;
			curveRightCounter = 0;
		} else { // geradeaus
			straightCounter++;

			if (isStraight()) {
				currentLinePos = idealLinePos;
				if (pid != pidStraight) {
					pid = pidStraight;
					pid.setSetpoint(0);
					BasicLogger.addLog("PIDSTRAIGHT");
				}
			} else
				currentLinePos = idealLinePos + linePosOffsetRight;

			curveLeftCounter = 0;
			curveRightCounter = 0;
		}
	}

	private boolean isStraight() {
		return straightCounter > minStraight;
	}

	private int calcFinalSteering(int angle, int linePos) {
		int diff = currentLinePos - linePos;
		diff = (diff / finalSteeringDividend);
		return angle - diff;
	}

	private void changeSpeed(int angle) {
		if (isStraight())
			communication.sendDirection(State.SLOW);
		else
			communication.sendDirection(State.SLOW);
	}
}
