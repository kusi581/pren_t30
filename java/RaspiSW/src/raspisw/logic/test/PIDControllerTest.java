package raspisw.logic.test;

import static org.junit.Assert.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import raspisw.logic.PIDController;

public class PIDControllerTest {

	private PIDController testee;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		testee = new PIDController(0, 0.1, 0, 0.3);

		ArrayList<Integer> values = new ArrayList<>();
		values.add(10);
		values.add(12);
		values.add(16);
		values.add(18);
		values.add(20);


		for (Integer v : values) {
				Double result = testee.update(System.nanoTime(), (double) v);
				System.out.println("v=" + v + ";result=" + result);
				Thread.sleep(50);
			
		}
	}

}
