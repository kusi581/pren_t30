package raspisw.logic;

import java.time.Instant;

public class LoggList {
	private Instant date;
	private String message;
	
	public LoggList(String message){
		this.message = message;
		this.date = Instant.now();
	}
	
	public Instant getInstant(){
		return this.date;
	}
	
	public String getMessage(){
		return this.message;
	}
}
