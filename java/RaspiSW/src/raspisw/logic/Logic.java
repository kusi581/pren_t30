package raspisw.logic;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import detection.ConfiguruartionValueHolder;
import detection.car.CarResult;
import detection.container.ContainerResult;
import detection.dump.DumpResult;
import detection.line.LineResult;
import raspisw.communication.Communication;
import raspisw.communication.ICommunication;
import raspisw.communication.IGPIOListener;
import raspisw.communication.State;
import raspisw.imageprocessing.Detection;
import raspisw.imageprocessing.DetectionType;
import raspisw.imageprocessing.IDetection;
import raspisw.imageprocessing.IResult;
import raspisw.imageprocessing.Result;

public class Logic implements ILogic, IGPIOListener, Runnable {

	private final boolean saveImages = false;
	private static Logic Instance;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(0);
	private boolean isRunning = false;
	private boolean isCarDetected = false;
	private boolean detectionIsContainer = false;
	private boolean tempoChangeAllowed = true;
	private byte containerCounter, sidewayCounter = 0;

	private int LOGICPROCESSINGRATEINMS = 50;
	private int lastAngle = 0;
	private int straightCounter = ConfiguruartionValueHolder.isUSE_LEFT_START() ? 5 : 0;
	private final int ANGLE_CORRECTION = -4;//-3;
	private final int centerDiffFactor = 35;
	private final double straightXPos = 0.4;
	private final double lineAngleDividend = 2.33333333333d;
	private final double xPosOffset = 0.1;
	private double centerLinePosition = straightXPos;
	private double lastXPosOfLine = 0;//centerLinePosition * centerDiffFactor;

	private final ICommunication communication;
	private final IDetection detection;
	private final IResult result;
	private final ICamera camera;
	private final LineProcessing lineProcessing;

	private Mat lastImage;
	private LineResult lastLine;
	private ContainerResult lastContainer;
	private CarResult lastCar;
	private DumpResult lastDump;

	private Logic() {
		// initialize all needed components
		communication = Communication.getInstance();
		communication.addGPIOListener(this);
		detection = Detection.getInstance();
		result = Result.getInstance();
		camera = Camera.getInstance();
		lineProcessing = new LineProcessing(communication);
		
		BasicLogger.addLog("LOGIC: Communication, Detection, Result, Camera initialized");
		BasicLogger.addLog("LOGIC: init finished");
	}

	/**
	 * Konstruktor f�r CommuniactionMok, ResultMok Wird von der Simulation
	 * verwendet!
	 * 
	 * @param communication
	 * @param result
	 */
	private Logic(final ICommunication communication, final ICamera camera, final IResult result) {
		// initialize all needed components
		this.communication = communication;
		lineProcessing = new LineProcessing(communication);
		communication.addGPIOListener(this);
		detection = Detection.getInstance();
		this.camera = camera;
		this.result = result;
	}

	public static Logic getInstance() {
		if (Instance == null) {
			Instance = new Logic();
		}
		return Instance;
	}

	public static Logic getInstance(final ICommunication communication, final ICamera camera, final IResult result) {
		if (Instance == null) {
			Instance = new Logic(communication, camera, result);
		}
		return Instance;
	}

	private int imgNr = 1;

	@Override
	public void run() {
		Mat image;

		// image detection
		image = camera.getCurrentImage();
		if (lastImage != image && image != null && image.width() > 0) {
			detection.detect(image.clone(), DetectionType.LINE);
			detection.detect(image.clone(), DetectionType.CONTAINER);
			detection.detect(image.clone(), DetectionType.DUMP);
			// detection.detect(image, DetectionType.CAR);

			if (saveImages) {
				final String fileName = "img_" + String.format("%04d", imgNr) + ".png";
				Imgcodecs.imwrite(fileName, image);
			}

			imgNr += 1;
			lastImage = image;
		}

		processCar();

		if (!isCarDetected) {
			final LineResult line = result.getNewestLine();
			if (line != null && line != lastLine)
			{
				lineProcessing.process(line);
				lastLine = line;
			}

			//if (containerCounter <= 2)
				//processContainer();
		}

		if (imgNr > 100 && imgNr % 5 == 0)
			processDump();
		if (imgNr % 50 == 0)
			System.gc();
	}

	@Override
	public void start() {
		camera.startCapture();

		isRunning = true;

		new Thread() {
			public void run() {
				Mat image;
				while(true)
				{
					image = camera.getCurrentImage();
					if (image != null && image.width() > 0)
						break;
				}
					
				detection.detect(image.clone(), DetectionType.LINE);
				detection.detect(image.clone(), DetectionType.CONTAINER);
			};
		}.start();
		// image detection

		if (ConfiguruartionValueHolder.isUSE_LEFT_START())
			startLeft();
		else
			startRight();

		scheduler.scheduleAtFixedRate(this, 0, LOGICPROCESSINGRATEINMS, TimeUnit.MILLISECONDS);
	}

	@Override
	public void stop() {
		scheduler.shutdown();

		isRunning = false;
		camera.stopCapture();
	}

	@Override
	public boolean isRunning() {
		return isRunning;
	}

	public void setLogicProcessingRateInMs(final int ms) {
		this.LOGICPROCESSINGRATEINMS = ms;
	}

	@Override
	public void inputRecieved() {
		BasicLogger.addLog("LOGIC: Event received from Communication");
		containerCounter++;
		communication.setDefaultPinState();
		communication.sendDirection(State.SLOW);
	}

	private void processLine() {
		final LineResult line = result.getNewestLine();

		if (line == null || line == lastLine)
			return;

		lastLine = line;
		final double lineCorrection = line.getAngle();

		if (lineCorrection < 50 && lineCorrection > -50) {
			BasicLogger.addLog("LOGIC: Angle between 50 and -50, ABORT");
			return;
		}
		// Effektiver Winkel der Linienerkennung umwandeln um damit rechnen zu
		// k�nnen
		int angleInt = getRecalculatedAngle(lineCorrection);

		// Versatz zum Winkel hinzuf�gen um Ungenauigkeit des Kameraaufbaus zu
		// korrigieren
		angleInt = angleInt + ANGLE_CORRECTION;

		// Winkel umrechnen um den effektiven Winkel den wir wollen auch fahren
		// k�nnen (Winkel f�r Kommunikation --> Arduino)
		int validAngle = correctingAngle(angleInt);

		int maxDiff = validAngle < -5 || validAngle > 5 ? 8 : 8;

		// Zu starke Abweichungen zum vorherigen Winkel herausfiltern
		if (Math.abs(lastAngle - validAngle) <= maxDiff) {
			lastAngle = validAngle;
		} else {
			validAngle = lastAngle;
		}

		changeCenterLinePosition(validAngle);

		// XPos. der n�chsten Linie filtern und Differenz zum centerLinePosition
		// mal centerDiffFactor hochrechnen
		final double diff = line.getxPosOfLine() < 0 ? 0
				: Math.round((line.getxPosOfLine() - centerLinePosition) * centerDiffFactor);

		// Zu starke Abweichungen zur vorherigen XPos Korrektur herausfiltern
        if (Math.abs(Math.abs(lastXPosOfLine) - Math.abs(diff)) <= 6) {
			validAngle += diff;
			lastXPosOfLine = diff;
		}
		else
			BasicLogger.addLog("LOGIC: !!! abweichung xpos = " + Math.abs(Math.abs(lastXPosOfLine) - Math.abs(diff)));
		

		// Ausgabe
		final NumberFormat f = new DecimalFormat("#0.0000");
		BasicLogger.addLog("LOGIC: (Angle=" + f.format(line.getAngle()) + ", (-15...15)=" + validAngle
				+ ", rawXpos=" + f.format(line.getxPosOfLine()) + " diff=" + f.format(diff) + " centerLinePosition= " + centerLinePosition +")");

		if (tempoChangeAllowed)
			changeSpeed(validAngle);

		// Steuersignal senden
		communication.sendSteering(sendAngle(validAngle));
	}

	private void changeSpeed(final int validAngle) {
		if (validAngle > -2 && validAngle < 2 && lastAngle > -2 && lastAngle < 2)
			communication.sendDirection(State.NORMAL);
		else if (validAngle > -4 && validAngle < 4 && lastAngle > -4 && lastAngle < 4)
			communication.sendDirection(State.NORMAL);
		else
			communication.sendDirection(State.NORMAL);
	}
	
	private void initTest(){

		communication.sendSteering(31);
		this.sleep(250);
		communication.sendSteering(1);
		this.sleep(250);
		communication.sendSteering(16);
		this.sleep(250);
	}

	// position front left corner of start field
	private void startRight() {
		initTest();
		communication.sendSteering(16);
		communication.sendDirection(State.TURBO);
		this.sleep(1500);

		communication.sendSteering(10);
		this.sleep(3000);
		
		communication.sendSteering(15);
		this.sleep(1000);
		
		communication.sendSteering(15);
		this.sleep(500);
	}

	// position front left corner of start field
	private void startLeft() {
		
		initTest();
		communication.sendSteering(16);
		communication.sendDirection(State.TURBO);
		this.sleep(1500);

		communication.sendSteering(10);
		this.sleep(3000);

		communication.sendSteering(16);
		this.sleep(2500);
	}

	private void sleep(final int milsec) {
		final Instant zeit = Instant.now().plusMillis(milsec);
		while (zeit.isAfter(Instant.now())) {
		}
	}

	private void changeCenterLinePosition(final int currentAngle) {
		if (currentAngle > 5) { // rechts
			centerLinePosition = straightXPos - xPosOffset;
			straightCounter = 0;
		} else if (currentAngle < -5) {// links
			centerLinePosition = straightXPos;
			straightCounter = 0;
		} else {// geradeaus
			straightCounter++;
			if (straightCounter > 3)
				centerLinePosition = straightXPos;
			else
				centerLinePosition = straightXPos - xPosOffset;
		}
	}

	private int sendAngle(final int angle) {
		int value = angle + 16;
		if (value < 1)
			value = 1;
		if (value > 31)
			value = 31;
		return value;
	}

	private int correctingAngle(final int angle) {
		// calculates the steering value from the detected angle
		final double calculated = (-90 + angle) / (lineAngleDividend);
		final int angleInt = (int) Math.round(calculated);
		return angleInt;
	}

	public int getRecalculatedAngle(final double raw) {
		int result = (int) Math.round(raw);

		if (result < 0)
			result = Math.abs(result);
		else
			result = 90 + (90 - result);

		return result;
	}

	private void processContainer() {
		final ContainerResult container = result.getNewestContainer();
		if (container != null && lastContainer != container) {
			lastContainer = container;
			
			if (validateContainer(container)) {
				BasicLogger.addLog("LOGIC: container detected");
				communication.sendDirection(State.STOP);
				communication.sendContainer();
				communication.setDefaultPinState();
			}
		}
	}

	private boolean validateContainer(final ContainerResult container) {
		boolean validationSignal = false;

		if (container.isContainerDetected()) {
			communication.sendDirection(State.SLOW);
			tempoChangeAllowed = false;
			detectionIsContainer = true;
		}

		if (detectionIsContainer && !container.isContainerDetected()) {
			validationSignal = true;
			detectionIsContainer = false;
			tempoChangeAllowed = true;
		}

		return validationSignal;
	}

	private void processDump() {
		DumpResult dump = result.getNewestDump();
		if (dump == null || lastDump == dump)
			return;

		//BasicLogger.addLog("LOGIC: finishDetected=" + dump.isFinishDetected());
		lastDump = dump;

		if (dump.isFinishDetected()) {
			sidewayCounter++;
			if (sidewayCounter >= 3) {
				sidewayCounter = 0;
				if (ConfiguruartionValueHolder.isUSE_LEFT_START()) {
					endLeft();
				} else {
					endRight();
				}
			}
		} else {
			sidewayCounter = 0;
		}
	}

	private void endLeft() {
		communication.sendDirection(State.TURBO);

		communication.sendSteering(16);
		this.sleep(1800);

		communication.sendSteering(8);
		this.sleep(2000);

		communication.sendSteering(16);
		this.sleep(2500);
		communication.sendDirection(State.STOP);
		//communication.sendDirection(State.ENDOFTRACK);
	}

	// TODO
	private void endRight() {
		communication.sendDirection(State.TURBO);

		communication.sendSteering(16);
		this.sleep(2800);

		communication.sendSteering(9);
		this.sleep(2000);

		communication.sendSteering(16);
		this.sleep(1355);
		communication.sendDirection(State.STOP);
		//communication.sendDirection(State.ENDOFTRACK);
	}

	// TODO
	private void processCar() {
		// BasicLogger.addLog("LOGIC: processCar executed --> not implemented
		// jet!");
		/*
		 * CarResult car = result.getNewestCar(); if (car == null || lastCar ==
		 * car) return;
		 * 
		 * lastCar = car;
		 * 
		 * while (car.isCarDetected()) { BasicLogger.addLog(
		 * "LOGIC: car detected"); BasicLogger.addLog("LOGIC: carResult = " +
		 * car)); isCarDetected = true; communication.sendDirection(State.STOP);
		 * }
		 */
	}
}