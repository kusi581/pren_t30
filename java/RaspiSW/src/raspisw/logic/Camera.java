package raspisw.logic;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

public class Camera implements ICamera, Runnable {
	private static Camera instance;

	// constants
	private int CapturingRateInMs = 100;
	private int imgNr = 1;

	private Mat currentImage;
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(0);
	private boolean isCapturing = false;
	private boolean shouldSaveImages = false;
	private VideoCapture camera;
	private Object lockObject;

	public static Camera getInstance() {
		return instance == null ? instance = new Camera() : instance;
	}

	private Camera() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		currentImage = new Mat();
	}

	public void startCapture() {
		camera = new VideoCapture();
		camera.open(0);
		camera.set(Videoio.CAP_PROP_FRAME_WIDTH, 640);
		camera.set(Videoio.CAP_PROP_FRAME_HEIGHT, 480);
		// for more settings:
		// http://stackoverflow.com/questions/11420748/setting-camera-parameters-in-opencv-python

		isCapturing = true;
		scheduler.scheduleAtFixedRate(this, 0, CapturingRateInMs, TimeUnit.MILLISECONDS);
		BasicLogger.addLog("Image capture started");
	}

	public void setShouldSaveImages(boolean shouldSave) {
		shouldSaveImages = shouldSave;
	}

	public void stopCapture() {
		scheduler.shutdown();
		camera.release();
		isCapturing = false;
		BasicLogger.addLog("Image capture stopped");
	}

	@Override
	public void run() {
		if (isCapturing && camera.isOpened()) {
			// get image from camera
			synchronized (this) {

				camera.read(currentImage);
				
				if (shouldSaveImages) {
					Imgcodecs.imwrite("img_" + imgNr + ".png", currentImage);
					imgNr += 1;
				}
			}
			// BasicLogger.addLog("Image captured on " + Instant.now());
		}
	}

	public Mat getCurrentImage() {
		Mat current;
		synchronized (this) {
			current = currentImage.clone();
		}
		return current;
	}
}
