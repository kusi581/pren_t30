/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspisw.communication;

/**
 *
 * @author Ramon Wyss
 */
public interface ICommunication {
	public void sendDirection(State motorSignal);

	public void sendSteering(int angle);

	public void sendContainer();

	public void addGPIOListener(IGPIOListener gpioListener);

	public void setDefaultPinState();

}
