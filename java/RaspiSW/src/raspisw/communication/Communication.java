package raspisw.communication;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import raspisw.logic.BasicLogger;

/**
 * @author Ramon
 * @version 2.0.0
 */
public class Communication implements ICommunication {
	private static Communication Instance;
	final GpioPinDigitalOutput pin22, pin26, pin27, pin1, pin24, pin28, pin29, pin5, pin25, pin6;
	final GpioPinDigitalInput pin0;
	final GpioController gpioDirection, gpioReciever, gpioSender, gpioSpeed, gpioReadFlag;
	private IGPIOListener gpioInputListener;

	protected Communication() {
		gpioDirection = GpioFactory.getInstance();
		gpioReciever = GpioFactory.getInstance();
		gpioSender = GpioFactory.getInstance();
		gpioSpeed = GpioFactory.getInstance();
		gpioReadFlag = GpioFactory.getInstance();

		pin0 = gpioReciever.provisionDigitalInputPin(RaspiPin.GPIO_00, "getControlPin", PinPullResistance.PULL_DOWN);
		pin1 = gpioSender.provisionDigitalOutputPin(RaspiPin.GPIO_01, "sendControlPin", PinState.LOW);
		pin6 = gpioReadFlag.provisionDigitalOutputPin(RaspiPin.GPIO_06, "ReadFlag", PinState.LOW);

		pin22 = gpioDirection.provisionDigitalOutputPin(RaspiPin.GPIO_22, "directionPin22", PinState.LOW);
		pin26 = gpioDirection.provisionDigitalOutputPin(RaspiPin.GPIO_26, "directionPin26", PinState.LOW);
		pin27 = gpioDirection.provisionDigitalOutputPin(RaspiPin.GPIO_27, "directionPin27", PinState.HIGH);
		pin24 = gpioDirection.provisionDigitalOutputPin(RaspiPin.GPIO_24, "directionPin24", PinState.LOW);
		pin25 = gpioDirection.provisionDigitalOutputPin(RaspiPin.GPIO_25, "directionPin25", PinState.LOW);

		pin28 = gpioSpeed.provisionDigitalOutputPin(RaspiPin.GPIO_28, "speedPin28", PinState.LOW);
		pin29 = gpioSpeed.provisionDigitalOutputPin(RaspiPin.GPIO_29, "speedPin29", PinState.LOW);
		pin5 = gpioSpeed.provisionDigitalOutputPin(RaspiPin.GPIO_05, "speedPin5", PinState.HIGH);

		initGPIOListener();

		pin28.setShutdownOptions(true, PinState.LOW);
		pin29.setShutdownOptions(true, PinState.LOW);
		pin5.setShutdownOptions(true, PinState.HIGH);
	}

	@Override
	public void setDefaultPinState() {
		pin6.low();
		pin22.low();
		pin26.low();
		pin27.high();
		pin1.low();
		pin24.low();
		pin28.low();
		pin29.low();
		pin5.high();
		pin25.low();
		pin6.high();
		BasicLogger.addLog("COMMUNICATION: setDefaultPinState()");
	}

	/**
	 * Call of this method sets value of GPIO's 28,29,24 according to signal
	 * received by logic component is used to send signals to MC according to
	 * implemented logic
	 * 
	 * @param motorSignal
	 *            READ-FLAG is set before all Settings are active! Stopping the
	 *            Car is more important than shutting down the controllers
	 * 
	 */
	@Override
	public void sendDirection(final State motorSignal) {
		if (motorSignal == lastSpeed)
			return;

		switch (motorSignal) {
		case BACKWARDS: {
			pin6.low();
			pin28.low();
			pin29.low();
			pin5.low();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State BACKWARDS)");
			break;
		}
		case STOP: {
			pin6.low();
			pin28.low();
			pin29.low();
			pin5.high();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State STOP)");
			break;
		}
		case SLOW: {
			pin6.low();
			pin28.high();
			pin29.low();
			pin5.high();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State SLOW)");
			break;
		}
		case NORMAL: {
			pin6.low();
			pin28.high();
			pin29.high();
			pin5.low();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State NORMAL)");
			break;
		}
		case TURBO: {
			pin6.low();
			pin28.high();
			pin29.high();
			pin5.high();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State TURBO)");
			break;
		}
		case ENDOFTRACK: {
			pin6.low();
			pin28.low();
			pin29.high();
			pin5.low();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State ENDOFTRACK)");
			break;
		}
		default: {
			pin6.low();
			pin28.low();
			pin29.low();
			pin5.high();
			pin6.high();
			BasicLogger.addLog("COMMUNICATION: sendDirection(State default)");
			break;
		}
		}
		lastSpeed = motorSignal;
	}

	/**
	 * This method is used to hand over control of vehicle to the MC Also stops
	 * the engines to avoid driving to far.
	 */
	@Override
	public void sendContainer() {
		pin6.low();
		sendDirection(State.STOP);
		pin1.high();
		pin6.high();
	}

	@Override
	public void addGPIOListener(final IGPIOListener gpioListener) {
		gpioInputListener = gpioListener;
	}

	/**
	 * If PinState changes from HIGH to LOW, then the container handling is
	 * completed.
	 */
	private void initGPIOListener() {
		pin0.addListener(new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(final GpioPinDigitalStateChangeEvent event) {
				BasicLogger.addLog("COMMUNICATION: EVENT: Pinstate changed, new PinState: " + pin0.getState());
				if (gpioInputListener != null) {
					if (pin0.getState() == PinState.LOW) {
						gpioInputListener.inputRecieved();
					}
				}
			}
		});
	}

	private State lastSpeed = State.STOP;

	/**
	 * @param angle
	 */
	@Override
	public void sendSteering(final int angle) {
		int value = angle;
		if (value < 1 && value > 31) {
			//System.out.println("Signal nicht erlaubt: " + value);
			value = 16;
		}

		//BasicLogger.addLog("COMMUNICATION: sendSteering(" + value + ")");

		// set pins low
		pin6.low();
		pin27.low();
		pin25.low();
		pin24.low();
		pin26.low();
		pin22.low();

		if (value >= 16) {
			pin27.high();
			value -= 16;
		}
		if (value >= 8) {
			pin25.high();
			value -= 8;
		}
		if (value >= 4) {
			pin24.high();
			value -= 4;
		}
		if (value >= 2) {
			pin26.high();
			value -= 2;
		}
		if (value >= 1) {
			pin22.high();
			value -= 1;
		}

		pin6.high();
	}

	public static Communication getInstance() {
		return Instance == null ? Instance = new Communication() : Instance;
	}
}