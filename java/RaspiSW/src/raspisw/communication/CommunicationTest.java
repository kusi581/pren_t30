package raspisw.communication;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CommunicationTest {
	private Communication testCommunication;
	private boolean assertionVar;

	public CommunicationTest() {
		testCommunication = Communication.getInstance();
	}

	@Before
	public void setUp() throws Exception {
		assertionVar = false;
		if (testCommunication.gpioSpeed.isShutdown() == true) {
			testCommunication = new Communication();
			testCommunication.setDefaultPinState();
			System.out.println("DefaultPinState");
		}
		testCommunication = Communication.getInstance();
	}

	@Test
	public void testDriveBackwards() throws Exception {
		testCommunication.sendDirection(State.BACKWARDS);
		if (testCommunication.pin6.isHigh() && testCommunication.pin28.isLow() && testCommunication.pin29.isLow()
				&& testCommunication.pin5.isLow() && testCommunication.pin6.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}

	@Test
	public void testDriveStop() throws Exception {
		testCommunication.sendDirection(State.STOP);
		if (testCommunication.pin6.isHigh() && testCommunication.pin28.isLow() && testCommunication.pin29.isLow()
				&& testCommunication.pin5.isHigh() && testCommunication.pin6.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}

	@Test
	public void testDriveSlow() throws Exception {
		testCommunication.sendDirection(State.SLOW);
		if (testCommunication.pin6.isHigh() && testCommunication.pin28.isHigh() && testCommunication.pin29.isLow()
				&& testCommunication.pin5.isHigh() && testCommunication.pin6.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}

	@Test
	public void testDriveNormal() throws Exception {
		testCommunication.sendDirection(State.NORMAL);
		if (testCommunication.pin6.isHigh() && testCommunication.pin28.isHigh() && testCommunication.pin29.isHigh()
				&& testCommunication.pin5.isLow() && testCommunication.pin6.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}

	@Test
	public void testDriveTurbo() throws Exception {
		testCommunication.sendDirection(State.TURBO);
		if (testCommunication.pin6.isHigh() && testCommunication.pin28.isHigh() && testCommunication.pin29.isHigh()
				&& testCommunication.pin5.isHigh() && testCommunication.pin6.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}

	@Test
	public void testSendSteering(int angle) throws Exception {
		testCommunication.sendSteering(angle);
	}

	@Test
	public void testDriveEOT() throws Exception {
		testCommunication.sendDirection(State.ENDOFTRACK);
		if (testCommunication.pin6.isHigh() && testCommunication.gpioSpeed.isShutdown() == true
				&& testCommunication.pin6.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}

	/*
	 * @Test public void testSteeringMR() throws Exception {
	 * testCommunication.sendSteering(Direction.MAX_RIGHT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isHigh() &&
	 * testCommunication.pin25.isHigh() && testCommunication.pin24.isHigh() &&
	 * testCommunication.pin26.isHigh() && testCommunication.pin22.isHigh() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 * 
	 * @Test public void testSteeringR() throws Exception {
	 * testCommunication.sendSteering(Direction.RIGHT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isHigh() &&
	 * testCommunication.pin25.isHigh() && testCommunication.pin24.isLow() &&
	 * testCommunication.pin26.isLow() && testCommunication.pin22.isLow() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 * 
	 * @Test public void testSteeringFR() throws Exception {
	 * testCommunication.sendSteering(Direction.FEW_RIGHT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isHigh() &&
	 * testCommunication.pin25.isLow() && testCommunication.pin24.isLow() &&
	 * testCommunication.pin26.isLow() && testCommunication.pin22.isHigh() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 * 
	 * @Test public void testSteeringStraight() throws Exception {
	 * testCommunication.sendSteering(Direction.STRAIGHT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isHigh() &&
	 * testCommunication.pin25.isLow() && testCommunication.pin24.isLow() &&
	 * testCommunication.pin26.isLow() && testCommunication.pin22.isLow() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 * 
	 * @Test public void testSteeringFL() throws Exception {
	 * testCommunication.sendSteering(Direction.FEW_LEFT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isLow() &&
	 * testCommunication.pin25.isHigh() && testCommunication.pin24.isHigh() &&
	 * testCommunication.pin26.isHigh() && testCommunication.pin22.isHigh() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 * 
	 * @Test public void testSteeringL() throws Exception {
	 * testCommunication.sendSteering(Direction.LEFT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isLow() &&
	 * testCommunication.pin25.isHigh() && testCommunication.pin24.isLow() &&
	 * testCommunication.pin26.isLow() && testCommunication.pin22.isHigh() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 * 
	 * @Test public void testSteeringML() throws Exception {
	 * testCommunication.sendSteering(Direction.MAX_LEFT); if
	 * (testCommunication.pin6.isHigh() && testCommunication.pin27.isLow() &&
	 * testCommunication.pin25.isLow() && testCommunication.pin24.isLow() &&
	 * testCommunication.pin26.isLow() && testCommunication.pin22.isHigh() &&
	 * testCommunication.pin6.isHigh()) { assertionVar = true; }
	 * assertTrue(assertionVar); }
	 */

	@Test
	public void testSendContainer() throws Exception {
		testCommunication.sendContainer();
		System.out.println("STOP successfully executed for ContainerHandling");
		if (testCommunication.pin6.isHigh() && testCommunication.pin1.isHigh() && testCommunication.pin28.isLow()
				&& testCommunication.pin29.isLow() && testCommunication.pin5.isHigh()) {
			assertionVar = true;
		}
		assertTrue(assertionVar);
	}
}