package raspisw.communication;

/**
 *
 * @author rawyss
 */
public enum State{
    BACKWARDS,
    STOP,
    SLOW,
    NORMAL,
    TURBO,
    ENDOFTRACK
}
