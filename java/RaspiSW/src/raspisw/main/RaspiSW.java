/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspisw.main;

import raspisw.logic.Logic;

/**
 *
 * @author kusi
 */
public class RaspiSW {

	/**
	 * @param args
	 *            the command line arguments
	 * @throws InterruptedException
	 */
	public static void main(final String[] args) throws InterruptedException {
		// BasicLogger.startSavingInterval(30);

		final Logic logicInstance = Logic.getInstance();
		while (true) {
			if (!logicInstance.isRunning())
				logicInstance.start();

			Thread.sleep(1000);

			/*
			 * TODO exception handling try { logicInstance.start(); } catch
			 * (Exception ex) { logicInstance.stop(); BasicLogger.addLog(
			 * "MAIN: Exception occured - Logic stopped"); Thread.sleep(5 *
			 * 1000); }
			 */
		}
	}
}