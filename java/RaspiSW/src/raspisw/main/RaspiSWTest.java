package raspisw.main;

import raspisw.communication.CommunicationTest;
import raspisw.logic.BasicLogger;
import raspisw.logic.ILogic;
import raspisw.logic.Logic;

public class RaspiSWTest {
	private static CommunicationTest commTest;

	public static void main(String[] args) throws InterruptedException {
		BasicLogger.startSavingInterval(1);
		commTest = new CommunicationTest();
		boolean logicTestingEnabled = false; // used to test LogicInstance
		ILogic logicInstance = Logic.getInstance();
		int y = 1000;
		try {
			BasicLogger.addLog("MAIN: Counter = " + y + " ms");
			commTest.setUp();
			Thread.sleep(y);
			BasicLogger.addLog("MAIN: Setup finished");
			BasicLogger.addLog("MAIN: Starting with generall Signalisation testing");
			commTest.testDriveBackwards();
			Thread.sleep(y);
			commTest.testDriveStop();
			Thread.sleep(y);
			commTest.testDriveSlow();
			Thread.sleep(y);
			commTest.testDriveStop();
			Thread.sleep(y);
			commTest.testDriveNormal();
			Thread.sleep(y);
			commTest.testDriveStop();
			Thread.sleep(y);
			commTest.testDriveTurbo();
			Thread.sleep(y);
			commTest.testDriveNormal();
			Thread.sleep(y);

			// straigth -> max right
			for (int i = 90; i >= 75; i--) {
				commTest.testSendSteering(i);
				Thread.sleep(y);
			}

			// straigth -> max left
			for (int i = -90; i <= -75; i++) {
				commTest.testSendSteering(i);
				Thread.sleep(y);
			}

			commTest.testDriveStop();
			Thread.sleep(y);
			commTest.testSendContainer();
			Thread.sleep(y);
			BasicLogger.addLog("MAIN: Exection-Counter: 0");
		} catch (Exception ex) {
			BasicLogger.addLog("MAIN: Error occured in SignalValidation " + ex);
		}

		try {
			BasicLogger.addLog("MAIN: Testing EOT");
			commTest.testDriveEOT();
			Thread.sleep(y);
		} catch (Exception ex) {
			BasicLogger.addLog("MAIN: Error occured in EOT " + ex);
		}

		BasicLogger.addLog("MAIN: Communication-UnitTests Finished");

		if (logicTestingEnabled) {
			BasicLogger.addLog("MAIN: LogicTesting started");
			logicInstance.start();
			// l�sst logic-Instance 1 Sekunde arbeiten
			Thread.sleep(1 * 1000);
			for (int i = 0; i >= 50; i++) {
				Thread.sleep(20);
			}
			logicInstance.stop();
			BasicLogger.addLog("MAIN: LogicTesting finished");
		}

		BasicLogger.stopLogger();
	}
}
