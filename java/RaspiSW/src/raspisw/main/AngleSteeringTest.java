package raspisw.main;

import org.opencv.core.Core;

import raspisw.logic.Logic;
import raspisw.simulation.gui.CameraMock;
import raspisw.simulation.gui.CommunicationMock;
import raspisw.simulation.gui.ResultMock;

public class AngleSteeringTest {

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Logic l = Logic.getInstance(new CommunicationMock(), new CameraMock(), new ResultMock());
		l.setLogicProcessingRateInMs(5);
		l.start();
		
		while(true){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}
}